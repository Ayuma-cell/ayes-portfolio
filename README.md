![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/Ayuma-cell/ayes-portfolio?branch=master&label=Build%20Status&style=for-the-badge)

---

A Portfolio Website made by Justin Schildt aka. Ayes.

Showing some of my recently acquired skills including but not limited to:

- Html knowledge
- Css styling
- Selectors
- Classes

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->